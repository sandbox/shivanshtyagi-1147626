<?php
/**
 * @license GNU General Public License version 2 (GPLv2)
 */
//Class containing common functions and constants
abstract class Common {
    protected $MOBSTAC_API_KEY = "mobstac_api_key";
    protected $MOBSTAC_BEHAVIOUR_VERSION = "2b";
    protected $MOBILE_URL = "mobile_url";

    private function get_tld($url) {
        preg_match('@^(?:http://)?([^/]+)@i', $url, $matches);
        $host = $matches[1];
        // get last two segments of host name
        preg_match('/[^.]+\.[^.]+$/', $host, $matches);
        if (count($matches) > 0) {
            return $matches[0];
        }
        else {
            preg_match('@^(?:http://)?([^/]+)@i', $url, $matches);
            return $matches[1];
        }
    }

    function mobstac_check_required_fields($mobile_url, $mobstac_api_key) {
        if ($mobile_url && $mobstac_api_key) {
            return true;
        } else {
            return false;
        }
    }

    function mobstac_ping() {
        $mobile_url = $mobstac_api_key = '';
        $this->mobstac_get_params($mobile_url, $mobstac_api_key);
        if ($this->mobstac_check_required_fields($mobile_url, $mobstac_api_key)) {
            $fh = @fopen($mobile_url.'/m/ping/'.'?v='.$this->MOBSTAC_BEHAVIOUR_VERSION.'&mak='.$mobstac_api_key, 'r');
            if (!$fh) {
                return;
            }
            @fclose($fh);
        }
    }

    //Pings MobStac if plugin is disabled
    function mobstac_plugin_disabled() {
        $mobile_url = $mobstac_api_key = '';
        $this->mobstac_get_params($mobile_url, $mobstac_api_key);
        if ($this->mobstac_check_required_fields($mobile_url, $mobstac_api_key)) {
            $fh = @fopen($mobile_url.'/m/plugindisabled/'.'?mak='.$mobstac_api_key, 'r');
            if (!$fh) {
                return;
            }
            @fclose($fh);
        }
    }

    private function mobstac_mane_param($complete_request_uri) {
        $maneref = '';
        if (isset($_SERVER['HTTP_REFERER'])) {
            $http_referer = urlencode(utf8_encode($_SERVER['HTTP_REFERER']));
            $maneref = 'maneref='.$http_referer;
        } else {
            return '';
        }

        if (strpos($complete_request_uri, '?') > 0) {
            return '&'.$maneref;
        }
            return '?'.$maneref;
    }
    
    function mobstac_handle_googlebot_mobile() {
        $mobile_url = $mobstac_api_key = '';
        $this->mobstac_get_params($mobile_url, $mobstac_api_key);
        if (!$this->mobstac_check_required_fields($mobile_url, $mobstac_api_key)) {
            return;
        }
        if (strpos($_SERVER['HTTP_USER_AGENT'], 'Googlebot-Mobile') > 0) {
            $complete_request_uri = utf8_encode($_SERVER['REQUEST_URI']);
            $maneref = $this->mobstac_mane_param($complete_request_uri);    
            if ($mobile_url != "") {
                $this->mobstac_redirect($mobile_site_url.$complete_request_uri.$maneref);
            }
        }
    }

    function mobstac_get_redirection_script() {
        $mobile_url = $mobstac_api_key = '';
        $this->mobstac_get_params($mobile_url, $mobstac_api_key);
        $script=<<<JS_SNIP1
        try {
            var MSTAC = new Object();
JS_SNIP1;
        $script .= 'MSTAC.murl = "'.$mobile_url.'";';
        $script .= 'MSTAC.d = ".'.$this->get_tld($mobile_url).'";';
        $script .= PHP_EOL;
        $script .= <<<JS_SNIP2
            if ((document.cookie.search("mstac=desktop") != -1) || (document.cookie.search("mstac_override=1") != -1)) {} 
            else if (window.location.search.search("mstac=0") != -1) {
                document.cookie = 'mstac_override=1; path=/; domain=' + MSTAC.d;
            } else {
                var ms = document.createElement('script'); ms.type = 'text/javascript';
                ms.src = MSTAC.murl + '/m/fastredirect/';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ms, s);
            }
        }
        catch(err) {}
JS_SNIP2;
        $script .= PHP_EOL;
        return $script;
    }

    abstract protected function mobstac_redirect($url);
    abstract protected function mobstac_get_params(&$mobile_url, &$mobstac_api_key);
    abstract protected function mobstac_is_admin();
    abstract protected function mobstac_admin_warnings();
    abstract protected function mobstac_insert_script();

    //API Functions
    abstract protected function mobstac_get_categories();
    abstract protected function mobstac_get_content($type='default');
    abstract protected function mobstac_get_api_version();
    abstract protected function mobstac_get_platform_version();
}
?>

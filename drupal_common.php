<?php
/**
 * Version: 2.0
 *
 * Description: Port of MobStac plugin to Drupal. This file contains concrete common API class. To be included
 *              with the main module.
 * Author:  Kushagra Sinha
 * Date:    8th June, 2011
 */

class MobstacDrupal extends Common {
    protected $MOBSTAC_API_VERSION = "2.0";

    function mobstac_redirect($url) {
        header('Location: '.$url);
    }

    function mobstac_get_params(&$mobile_url, &$mobstac_api_key) {
        $mobile_url = variable_get('mobile_url', '');
        $mobstac_api_key = variable_get('mobstac_api_key', '');
    }

    function mobstac_is_admin() {
        if (user_access('administer')) {
            return true;
        }
        else {
            return false;
        }
    }

    function mobstac_admin_warnings() {
        if (user_access('administer')) {
            $mobile_url = $mobstac_api_key = '';
            $this->mobstac_get_params($mobile_url, $mobstac_api_key);
            if (!$this->mobstac_check_required_fields($mobile_url, $mobstac_api_key)) {
                drupal_set_message(t('Kindly configure the MobStac plugin'), 'warning');
            }
        }
    }

    function mobstac_insert_script() {
        $script = $this->mobstac_get_redirection_script();
        drupal_add_js($script, 'inline');
        $mobile_url = $mobstac_api_key = '';
        $this->mobstac_get_params($mobile_url, $mobstac_api_key);
        $permapath = $_SERVER['REQUEST_URI'];
        $element = array(
                '#tag'          => 'link',
                '#attributes'   => array(
                    'href'  => $mobile_url.$permapath,
                    'rel'   => 'alternate',
                    'media' => 'handheld',
                    'type'  => 'text/html'
                )
        );
        drupal_add_html_head($element, 'mobstac_drupal_link_url');
    }

    //API Functions
    function mobstac_api_handler() {
	    // any new api methods should be added to this list
        $MOBSTAC_API_DEFINED_METHODS = array("getContent", "getCategories", "getApiVersion", "getPlatformVersion");
        $mobile_url = $mobstac_api_key = '';
        $this->mobstac_get_params($mobile_url, $mobstac_api_key);

        //check if the url is a mobstac api url
        if (!(strpos(strtolower($_SERVER['REQUEST_URI']), 'mobstac-api') > 0)) {
            return;
        }
        //check if the required plugin params are set
        if (!$this->mobstac_check_required_fields($mobile_url, $mobstac_api_key)) {
            header("HTTP/1.1 500 Server Error");
            header("Status: 500 Server Error");
            echo '{"message" :"MobStac configuration incomplete!"}';
            exit();
        }
        //authenticate the api key
        if ($_GET['apikey'] != $mobstac_api_key) {
            header("HTTP/1.1 403 Forbidden");
            header("Status: 403 Forbidden");
            echo '{"message" : "API Key value does not match"}';
            exit();
        }
        //check if the request is for a valid method
        if (!in_array($_GET['method'], $MOBSTAC_API_DEFINED_METHODS)) {
            header("HTTP/1.1 501 Method Not Implemented");
            header("Status: 501 Method Not Implemented");
            echo '{"message" : "The requested method is yet to be implemented."}';
            exit();
        }
        //getMobstacVersion api method implementation
        if ($_GET['method'] == 'getApiVersion') {
            $this->mobstac_get_api_version();
        }
        if ($_GET['method'] == 'getPlatformVersion') {
            $this->mobstac_get_platform_version();
        }
        if ($_GET['method'] == 'getContent') {
	    	if (!isset($_GET['type'])) {
	    	    $type = 'default';
	    	}
	    	else {
	    	    $type = $_GET['type'];
    	    }
	    	$this->mobstac_get_content($type);
        }
        //getCategories api method implementation
        if ($_GET['method'] == 'getCategories') {
	        $this->mobstac_get_categories();
        }
	}

    function mobstac_get_categories() {
        $result = db_select('node_type', 'nt')
            ->fields('nt')
            ->execute();
        echo FastJSON::encode($result->fetchAll());
        exit();
    }

    function mobstac_get_content($type='default') {
        if ($type === 'default') {
            $type = 'article';
        }
        else if ($type === 'alternate') {
            $type = 'page';
        }
        $query = db_query("SELECT nid FROM {node} WHERE type = :type AND status = 1", array(':type' => $type));
        $max_number = $query->rowCount();
        if (!$max_number) {
            header("HTTP/1.1 400 Bad Request");
            header("Status: 400 Bad Request");
            echo '{"message" : "Content type empty or nonexistent"}';
            exit();
        }
        if (isset($_GET['count'])) {
            // check if count is a numeric value greater than zero 
            if ((!is_numeric($_GET['count'])) || (int)$_GET['count'] < 1) {
                header("HTTP/1.1 400 Bad Request");
                header("Status: 400 Bad Request");
                echo '{"message" : "count should be a number greater than 0"}';
                exit();
            }
            $number = (int)$_GET['count'];
            if ($number > $max_number) {
                header("HTTP/1.1 400 Bad Request");
                header("Status: 400 Bad Request");
                echo '{"message" : "The max value of count can be '.$max_number.'"}';
                exit();
            }
        } else {
            $number = 5;
        }
        if (isset($_GET['offset']) && is_numeric($_GET['offset'])) {
            $offset = (int)$_GET['offset'];
            $total_number = $number + $offset;
            if (isset($_GET['count'])) {
                if($total_number > $max_number) {
                   header("HTTP/1.1 400 Bad Request");
                   header("Status: 400 Bad Request");
                   echo '{"message" : "offset value plus count can not be more than '.$max_number.'"}';
                   exit();
                }
            }
        } else {
            $offset = 0;
        }
        $query = db_query_range("SELECT * FROM {node} WHERE type = :type AND status = 1", $offset, $number, array(':type' => $type));
        $results = $query->fetchAll();

        global $base_url;
        $base = $base_url.'/node/';
        $query = db_query("SELECT entity_id, body_value FROM {field_data_body}");

        $tmp = $query->fetchAll();
        $contents = array();
        foreach ($tmp as $key => $value) {
            $contents[$tmp[$key]->entity_id] = $tmp[$key]->body_value;
        }
        $date = new DateTime();
        $date->setTimezone(new DateTimeZone('UTC'));
        foreach ($results as $key => $value) {
            $results[$key]->rssLink = $base.$results[$key]->nid;
            $results[$key]->content = $contents[$results[$key]->nid];
            $date->setTimestamp($results[$key]->changed);
            $results[$key]->updated = $date->format('Y-m-d H:i:s');
        }
        header("HTTP/1.1 200 OK");
        header("Status: 200 OK");
        echo FastJSON::encode($results);
        exit();
    }

    function mobstac_get_api_version() {
        header("HTTP/1.1 200 OK");
        header("Status: 200 OK");    
        echo '{"version" : "'.$this->MOBSTAC_API_VERSION.'"}';
        exit();
    }

    function mobstac_get_platform_version() {
        header("HTTP/1.1 200 OK");
        header("Status: 200 OK");    
        echo '{"platform" : "Drupal" , "version" : "'.VERSION.'"}';
        exit();
    }
}
?>
